﻿using System;

class Minimax
{
    public readonly struct MinimaxResult
    {
        public MinimaxResult(long value, Move move)
        {
            Value = value;
            SelectedMove = move;
        }
        public long Value { get; }
        public Move SelectedMove { get; }
    }

    public static MinimaxResult Solve(Board board, Player player, Player opponent, int levels)
    {
        return Minimax.Solve(board, player, opponent, levels, true, Int64.MinValue, Int64.MaxValue, null);
    }

    public static MinimaxResult Solve(Board board, Player player, Player opponent, int levels, bool maximizing, long alpha, long beta, Move move)
    {
        MinimaxResult result;

        if (levels == 0 || board.CheckEnded())
        {
            long score = board.CalculateScore(player, opponent) * (maximizing ? 1 : -1);
            result = new MinimaxResult(score, move);
        }
        else if (maximizing)
        {
            MinimaxResult partialResult;
            MinimaxResult best = new MinimaxResult(Int64.MinValue, null);
            int i = 0;
            bool stop = false;
            Move[] validMoves = board.GetValidMoves(player);
            while (!stop && i < validMoves.Length)
            {
                Move moveToAnalyze = validMoves[i];
                partialResult = Minimax.Solve(board.MakeMove(player, moveToAnalyze, false), opponent, player, levels - 1, !maximizing, alpha, beta, moveToAnalyze);
                if (partialResult.Value > best.Value)
                {
                    best = new MinimaxResult(partialResult.Value, moveToAnalyze);
                }
                alpha = Math.Max(alpha, best.Value);
                stop = (alpha >= beta);
                i++;
            }
            result = best;
        }
        else
        {
            MinimaxResult partialResult;
            MinimaxResult best = new MinimaxResult(Int64.MaxValue, null);
            int i = 0;
            bool stop = false;
            Move[] validMoves = board.GetValidMoves(player);
            while (!stop && i < validMoves.Length)
            {
                Move moveToAnalyze = validMoves[i];
                partialResult = Minimax.Solve(board.MakeMove(player, moveToAnalyze, false), opponent, player, levels - 1, !maximizing, alpha, beta, moveToAnalyze);
                if (partialResult.Value < best.Value)
                {
                    best = new MinimaxResult(partialResult.Value, moveToAnalyze);
                }
                beta = Math.Min(beta, best.Value);
                stop = (beta <= alpha);
            }
            result = best;
        }
        return result;
    }
}
