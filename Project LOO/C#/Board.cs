﻿interface Board
{
    long CalculateScore(Player player, Player opponent);
    bool CheckEnded();
    Move[] GetValidMoves(Player player);
    Board MakeMove(Player player, Move move, bool inplace);
    bool CheckWinner(Player player);
}
