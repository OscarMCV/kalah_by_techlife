﻿interface Game
{
    Board GetBoard();
    Player GetPlayerInTurn();
    bool CheckEnded();
    Player GetWinner();
    void ChangeTurn();
    bool MakeHumanMove(Move move);
    Move MakeAiMove();
}
