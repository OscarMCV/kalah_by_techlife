﻿
class Player
{
    private string name;
    private bool kind;
    private bool status;

    public Player(bool kind = false, string name = "")
    {
        this.kind = kind;
        if (name != "player1" && name != "player2")
        {
            throw new Exception("No puedes intanciar un jugador con nombre inapropiado.");
            this.status = false;
        }
        else
        {
            this.name = name;
            this.status = true;
        }
    }

    public string Name
    {
        get
        {
            return this.name;
        }
    }

    public bool kind
    {
        get
        {
            return this.kind;
        }
    }

    public bool Status
    {
        get
        {
            return this.name;
        }
    }
}
